# 健康数据管理

## 项目实施意义

随着现代社会物联网科技及医疗水平迅速发展，智慧医疗的趋势也越发明显，但我国公共医疗管理系统的不完善，医疗成本高、渠道少、覆盖面低、资源分配不均等问题依然困扰着许多人。根据最新《中国健康调查报告》显示我国国民患病率仍处于持续增加状态，国民健康意识薄弱。所以我们不应该只把目光聚集到如何提高医疗水平上，而是应该防患于未然，去了解健康、医学常识并实时的关注、监测自身的心率、脉搏等生理健康状况，做好疾病预防工作。本项目就是为实现此目的而申请的。

## 依赖

- 云开发
- Vue

## 项目体验

微信小程序搜索“健康数据管理”

## 项目部署指南

1. 下载本项目或者git clone https://gitee.com/tysb7/WeChat-app-hdm
2. 修改 /project.config.json中appid
3. 修改 /dist/project.config.json中appid 
2. 导入微信小程序开发工具
5. 创建云数据库`userData`
6. 运行Vue

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

```


## 开发说明

[开发说明 >>](https://gitee.com/tysb7/WeChat-app-hdm/blob/master/contributing.md)

## Bug 反馈

如果有Bug，请提交[issues >>](https://gitee.com/tysb7/WeChat-app-hdm/issues/new)反馈。

## 联系方式

- WeChat：tangyuantang
- QQ：917755100

## 更新日志


## 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0216/230309_10366b9a_1463122.png)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0216/230320_bd27fba1_1463122.png)

## 开源协议LICENSE

[Apache-2.0](https://gitee.com/tysb7/tcb-hackthon-YT/blob/master/LICENSE)





