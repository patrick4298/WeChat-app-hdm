require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([3],{

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(27);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_36b52cec_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(30);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(28)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-36b52cec"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_36b52cec_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/addDate/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-36b52cec", Component.options)
  } else {
    hotAPI.reload("data-v-36b52cec", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 28:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var db = wx.cloud.database();

/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      tabs: ['体重', '血压', '检测(Beta)'],
      activeIndex: 0,
      fontSize: 30
    };
  },

  computed: {
    navbarSliderClass: function navbarSliderClass() {
      if (this.activeIndex === 0) {
        return 'weui-navbar__slider_0';
      }
      if (this.activeIndex === 1) {
        return 'weui-navbar__slider_1';
      }
      if (this.activeIndex === 2) {
        return 'weui-navbar__slider_2';
      }
    }
  },
  methods: {
    tabClick: function tabClick(e) {
      console.log(e);
      this.activeIndex = Number(e.currentTarget.id);
    },
    formSubmit: function formSubmit(e) {
      var data = e.mp.detail.value;
      // 获取今日日期
      var time = new Date();
      var date = new Date();
      // var y = time.getFullYear();
      // var m = time.getMonth()+1;
      // var d = time.getDate();
      // let date = new Date(y.toString()+"-"+m.toString()+"-"+d.toString());

      //data = data.concat(time,date)

      if (data.weight == '') {
        wx.vibrateShort();
        wx.showToast({
          title: "请填写有效体重",
          icon: 'none',
          duration: 2000
        });
      } else if (data.SP == '') {
        wx.vibrateShort();
        wx.showToast({
          title: "请填写有效收缩压",
          icon: 'none',
          duration: 2000
        });
      } else if (data.DP == '') {
        wx.vibrateShort();
        wx.showToast({
          title: "请填写有效舒张压",
          icon: 'none',
          duration: 2000
        });
      } else if (data.Pulse == '') {
        wx.vibrateShort();
        wx.showToast({
          title: "请填写有效脉搏",
          icon: 'none',
          duration: 2000
        });
      } else {
        switch (this.activeIndex) {
          case 0:
            var userData = {
              mode: 0,
              time: time,
              date: date,
              weight: data.weight
            };
            break;
          case 1:
            var userData = {
              mode: 1,
              time: time,
              date: date,
              DP: data.DP,
              SP: data.SP,
              Pulse: data.Pulse
            };
            break;
        }

        db.collection('userData').add({
          // data 字段表示需新增的 JSON 数据
          data: userData
        }).then(function (res) {
          if (res.errMsg == 'collection.add:ok') {
            wx.vibrateShort();
            wx.showToast({
              title: "添加成功！",
              icon: 'success',
              duration: 2000
            });
            var timer;
            timer = setTimeout(function () {
              wx.navigateBack({
                delta: 1
              });
              clearTimeout(timer);
            }, 1500);
          } else {
            wx.vibrateShort();
            wx.showToast({
              title: "添加失败！",
              icon: 'loading',
              duration: 2000
            });
          }
        });
      }

      console.log('form发生了submit事件，携带数据为：', e.mp.detail.value);
    }
  }
});

/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "page",
    staticStyle: {
      "height": "100%"
    }
  }, [_c('div', {
    staticClass: "page__bd"
  }, [_c('div', {
    staticClass: "weui-tab"
  }, [_c('div', {
    staticClass: "weui-navbar"
  }, [_vm._l((_vm.tabs), function(item, index) {
    return _c('block', {
      key: index
    }, [_c('div', {
      staticClass: "weui-navbar__item",
      class: {
        'weui-bar__item_on': _vm.activeIndex == index
      },
      attrs: {
        "id": index,
        "eventid": '0-' + index
      },
      on: {
        "click": _vm.tabClick
      }
    }, [_vm._v("\n            " + _vm._s(item) + "\n          ")])])
  }), _vm._v(" "), _c('div', {
    staticClass: "weui-navbar__slider",
    class: _vm.navbarSliderClass
  })], 2), _vm._v(" "), _c('div', {
    staticClass: "weui-tab__panel"
  }, [_c('div', {
    staticClass: "weui-tab__content",
    attrs: {
      "hidden": _vm.activeIndex != 0
    }
  }, [_c('form', {
    staticClass: "weui-cell__bd",
    attrs: {
      "eventid": '1'
    },
    on: {
      "submit": _vm.formSubmit
    }
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("体重")]), _vm._v(" "), _c('input', {
    staticClass: "weui-input input",
    attrs: {
      "name": "weight",
      "type": "digit",
      "placeholder": "请输入体重(Kg)"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "weui-btn btn",
    attrs: {
      "form-type": "submit"
    }
  }, [_vm._v("提交")])], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "weui-tab__content",
    attrs: {
      "hidden": _vm.activeIndex != 1
    }
  }, [_c('form', {
    staticClass: "weui-cell__bd",
    attrs: {
      "eventid": '2'
    },
    on: {
      "submit": _vm.formSubmit
    }
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("收缩压")]), _vm._v(" "), _c('input', {
    staticClass: "weui-input input",
    attrs: {
      "name": "SP",
      "type": "digit",
      "placeholder": "请输入收缩压(mmHg)"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v("舒张压")]), _vm._v(" "), _c('input', {
    staticClass: "weui-input input",
    attrs: {
      "name": "DP",
      "type": "digit",
      "placeholder": "请输入舒张压(mmHg)"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v("脉搏")]), _vm._v(" "), _c('input', {
    staticClass: "weui-input input",
    attrs: {
      "name": "Pulse",
      "type": "digit",
      "placeholder": "请输入脉搏(bpm)"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "weui-btn btn",
    attrs: {
      "form-type": "submit"
    }
  }, [_vm._v("提交")])], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "weui-tab__content",
    attrs: {
      "hidden": _vm.activeIndex != 2
    }
  })])])])])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-36b52cec", esExports)
  }
}

/***/ })

},[26]);
//# sourceMappingURL=main.js.map