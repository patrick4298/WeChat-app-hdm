require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([1],{

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(76);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_5daef28f_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(92);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(77)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_5daef28f_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/index/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5daef28f", Component.options)
  } else {
    hotAPI.reload("data-v-5daef28f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 77:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mpvue_calendar__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_mpvue_calendar_src_style_css__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_mpvue_calendar_src_style_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_mpvue_calendar_src_style_css__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var db = wx.cloud.database();
var _ = db.command;

/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      scrollHeight: 0,
      userData: [],
      showAddDate: true,
      todayData: [],
      //日历日期
      calendarData: [2019, 1, 1],
      //日历日期2
      calendarData2: [],
      range: false,
      weekSwitch: true,
      disabledArray: ["2019-2-27", "2019-3-2"],
      begin: [2019, 1, 1],
      end: [2022, 12, 31]
      // tileContent: [
      //     {date: '2019-3-2', className: 'holiday ', content: '休'},
      //     {date: '2018-9-23', className: 'holiday ', content: '休'}
      // ]
    };
  },


  components: {
    Calendar: __WEBPACK_IMPORTED_MODULE_0_mpvue_calendar__["a" /* default */]
  },
  computed: {
    showBtn: function showBtn() {
      var dateMin = new Date(this.calendarData + " " + 0 + ":" + 0 + ":" + 0);
      var dateMax = new Date(this.calendarData2 + " " + 23 + ":" + 59 + ":" + 59);
      //获取所选日期数据
      this.getData(dateMin, dateMax);
      //是否今日，打开上传数据按钮
      if (this.calendarData == this.todayData) {
        this.showAddDate = true;
      } else {
        this.showAddDate = false;
      }
    }
  },
  methods: {
    prev: function prev(year, month, weekIndex) {
      console.log(year, month, weekIndex);
    },
    next: function next(year, month, weekIndex) {
      console.log(year, month, weekIndex);
    },
    selectYear: function selectYear(year) {
      console.log(year);
    },
    selectMonth: function selectMonth(month, year) {
      console.log(year, month);
    },
    setToday: function setToday() {
      this.$refs.calendar.setToday();
      console.log();
    },
    dateInfo: function dateInfo() {
      var info = this.$refs.calendar.dateInfo(2018, 8, 23);
      console.log(info);
    },
    renderer: function renderer() {
      this.$refs.calendar.renderer(2018, 8); //渲染2018年8月份
    },
    select: function select(val, val2) {
      console.log(val);
      console.log(val2);
      this.calendarData = val[0] + "/" + val[1] + "/" + val[2];
      if (val2.date) {
        this.calendarData2 = val[0] + "/" + val[1] + "/" + val[2];
      } else {
        this.calendarData2 = val2[0] + "/" + val2[1] + "/" + val2[2];
      }
    },
    setRange: function setRange() {
      this.range = !this.range;
    },
    addDate: function addDate() {
      wx.navigateTo({
        url: "/pages/addDate/main"
      });
    },
    getData: function getData(dateMin, dateMax) {
      var _this = this;

      db.collection("userData").orderBy("mode", "asc").orderBy("time", "asc").where({
        date: _.lte(dateMax).and(_.gte(dateMin)),
        _openid: wx.getStorageSync('openid')
      }).get({
        success: function success(res) {
          var userData = res.data;
          for (var i = 0; i < userData.length; i++) {
            var M = userData[i].time.getMonth() + 1 + "/";
            var D = userData[i].time.getDate() + " ";
            var h = userData[i].time.getHours() + ":";
            var m = userData[i].time.getMinutes();
            var s = userData[i].time.getSeconds();
            userData[i].time = M + D + h + m;
          }
          // res.data 是包含以上定义的两条记录的数组
          _this.userData = res.data;
        }
      });
    },
    goToChart: function goToChart(e) {
      var url = "/pages/chart/main?mode=" + e.currentTarget.id + "&todayData=" + this.todayData;
      wx.navigateTo({ url: url });
    }
  },

  created: function created() {},
  onLoad: function onLoad() {
    var _this2 = this;

    // 获取今日日期
    var time = new Date();
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var date = y + "/" + m + "/" + d;
    this.todayData = y + "/" + m + "/" + d;
    //初始化今日日期
    this.calendarData = date;
    this.calendarData2 = date;

    //获取openid
    wx.cloud.callFunction({
      name: "login",
      complete: function complete(res) {
        wx.setStorageSync('openid', res.result.openid);
      }
    });

    var query = wx.createSelectorQuery();
    query.select("#head").boundingClientRect();
    query.exec(function (res) {
      var top = res[0].bottom;
      wx.getSystemInfo({
        success: function success(res) {
          var windowHeight = res.windowHeight;
          _this2.scrollHeight = windowHeight - top + res.statusBarHeight;
        }
      });
    });
  }
});

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_mpvue_loader_lib_selector_type_script_index_0_mpvue_calendar_vue__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mpvue_loader_lib_template_compiler_index_id_data_v_5b4e0b96_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_mpvue_loader_lib_selector_type_template_index_0_mpvue_calendar_vue__ = __webpack_require__(90);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(80)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_mpvue_loader_lib_selector_type_script_index_0_mpvue_calendar_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__mpvue_loader_lib_template_compiler_index_id_data_v_5b4e0b96_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_mpvue_loader_lib_selector_type_template_index_0_mpvue_calendar_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules/mpvue-calendar/src/mpvue-calendar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mpvue-calendar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5b4e0b96", Component.options)
  } else {
    hotAPI.reload("data-v-5b4e0b96", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 80:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_json_stringify__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_json_stringify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_json_stringify__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__calendarinit_js__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__icon_css__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__icon_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__icon_css__);



//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var isBrowser = !!window;
var now = new Date();
var todayString = [now.getFullYear(), now.getMonth() + 1, now.getDate()].join('-');
/* harmony default export */ __webpack_exports__["a"] = ({
  props: {
    multi: {
      type: Boolean,
      default: false
    },
    arrowLeft: {
      type: String,
      default: ''
    },
    arrowRight: {
      type: String,
      default: ''
    },
    clean: {
      type: Boolean,
      default: false
    },
    now: {
      type: [String, Boolean],
      default: true
    },
    range: {
      type: Boolean,
      default: false
    },
    completion: {
      type: Boolean,
      default: false
    },
    value: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    begin: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    end: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    zero: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    almanacs: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    tileContent: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    lunar: {
      type: Boolean,
      default: false
    },
    monFirst: {
      type: Boolean,
      default: false
    },
    weeks: {
      type: Array,
      default: function _default() {
        return this.monFirst ? ['一', '二', '三', '四', '五', '六', '日'] : ['日', '一', '二', '三', '四', '五', '六'];
      }
    },
    months: {
      type: Array,
      default: function _default() {
        return ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
      }
    },
    events: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    weekSwitch: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      years: [],
      yearsShow: false,
      year: 0,
      month: 0,
      monthPosition: 0,
      day: 0,
      days: [],
      multiDays: [],
      today: [],
      handleMultiDay: [],
      firstRender: true,
      isIos: true,
      showToday: {},
      monthText: '',
      festival: {
        lunar: {
          "1-1": "春节",
          "1-15": "元宵节",
          "2-2": "龙头节",
          "5-5": "端午节",
          "7-7": "七夕节",
          "7-15": "中元节",
          "8-15": "中秋节",
          "9-9": "重阳节",
          "10-1": "寒衣节",
          "10-15": "下元节",
          "12-8": "腊八节",
          "12-23": "小年"
        },
        gregorian: {
          "1-1": "元旦",
          "2-14": "情人节",
          "3-8": "妇女节",
          "3-12": "植树节",
          "5-1": "劳动节",
          "5-4": "青年节",
          "6-1": "儿童节",
          "7-1": "建党节",
          "8-1": "建军节",
          "9-10": "教师节",
          "10-1": "国庆节",
          "12-24": "平安夜",
          "12-25": "圣诞节"
        }
      },
      rangeBegin: [],
      rangeEnd: [],
      multiDaysData: [],
      monthsLoop: [],
      itemStyle: {},
      unit: isBrowser ? 'px' : 'rpx',
      positionH: isBrowser ? -24 : -40,
      monthIndex: 0,
      oversliding: false,
      rangeBgHide: false,
      monthDays: [],
      weekIndex: 0,
      startWeekIndex: 0,
      positionWeek: true
    };
  },

  watch: {
    events: function events() {
      this.render(this.year, this.month, '_WATCHRENDER_', 'events');
    },
    disabled: function disabled() {
      this.render(this.year, this.month, '_WATCHRENDER_', 'disabled');
    },
    value: function value() {
      this.render(this.year, this.month, '_WATCHRENDERVALUE_');
    },
    tileContent: function tileContent() {
      this.render(this.year, this.month, '_WATCHRENDER_', 'tileContent');
    },
    almanacs: function almanacs() {
      this.render(this.year, this.month, '_WATCHRENDER_', 'almanacs');
    }
  },
  created: function created() {
    var loopArray = this.months.concat();
    loopArray.unshift(this.months[this.months.length - 1]);
    loopArray.push(this.months[0]);
    this.monthsLoop = loopArray;
    this.monthsLoopCopy = this.monthsLoop.concat();
  },
  mounted: function mounted() {
    var self = this;
    var calendar = this.$refs.calendar;
    var itemWidth = (calendar.clientWidth / 7 - 4).toFixed(5);
    this.itemStyle = { width: itemWidth + 'px', height: itemWidth + 'px', lineHeight: itemWidth - 8 + 'px' };
    if (!isBrowser) {
      wx.getSystemInfo({
        success: function success(res) {
          self.isIos = (res.system.split(' ') || [])[0] === 'iOS';
        }
      });
    }
    this.oversliding = true;
    this.initRender = true;
    this.init();
  },

  methods: {
    init: function init() {
      var now = new Date();
      this.year = now.getFullYear();
      this.month = now.getMonth();
      this.day = now.getDate();
      this.monthIndex = this.month + 1;
      if (this.value.length || this.multi) {
        if (this.range) {
          this.year = parseInt(this.value[0][0]);
          this.month = parseInt(this.value[0][1]) - 1;
          this.day = parseInt(this.value[0][2]);
          var year2 = parseInt(this.value[1][0]);
          var month2 = parseInt(this.value[1][1]) - 1;
          var day2 = parseInt(this.value[1][2]);
          this.rangeBegin = [this.year, this.month, this.day];
          this.rangeEnd = [year2, month2, day2];
        } else if (this.multi) {
          this.multiDays = this.value;
          var handleMultiDay = this.handleMultiDay;
          if (this.firstRender) {
            this.firstRender = false;
            var thatYear = (this.value[0] || [])[0];
            var thatMonth = (this.value[0] || [])[1];
            if (isFinite(thatYear) && isFinite(thatMonth)) {
              this.month = parseInt(thatMonth) - 1;
              this.year = parseInt(thatYear);
            }
          } else if (this.handleMultiDay.length) {
            this.month = parseInt(handleMultiDay[handleMultiDay.length - 1][1]) - 1;
            this.year = parseInt(handleMultiDay[handleMultiDay.length - 1][0]);
            this.handleMultiDay = [];
          } else {
            this.month = parseInt(this.value[this.value.length - 1][1]) - 1;
            this.year = parseInt(this.value[this.value.length - 1][0]);
          }
          this.day = parseInt((this.value[0] || [])[2]);
        } else {
          this.year = parseInt(this.value[0]);
          this.month = parseInt(this.value[1]) - 1;
          this.day = parseInt(this.value[2]);
        }
      }
      this.updateHeadMonth();
      this.render(this.year, this.month);
    },
    renderOption: function renderOption(year, month, i, playload) {
      var weekSwitch = this.weekSwitch;
      var seletSplit = this.value;
      var isMonthModeCurrentMonth = !weekSwitch && !playload;
      var disabledFilter = function disabledFilter(disabled) {
        return disabled.find(function (v) {
          var dayArr = v.split('-');
          return year === Number(dayArr[0]) && month === dayArr[1] - 1 && i === Number(dayArr[2]);
        });
      };
      if (this.range) {
        var lastDay = new Date(year, month + 1, 0).getDate() === i ? { lastDay: true } : null;
        var options = __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()({ day: i }, this.getLunarInfo(year, month + 1, i), this.getEvents(year, month + 1, i), lastDay);
        var date = options.date,
            day = options.day;

        var copyRangeBegin = this.rangeBegin.concat();
        var copyRangeEnd = this.rangeEnd.concat();
        copyRangeBegin[1] = copyRangeBegin[1] + 1;
        copyRangeEnd[1] = copyRangeEnd[1] + 1;
        if (weekSwitch || isMonthModeCurrentMonth) {
          copyRangeEnd.join('-') === date && (options.rangeClassName = 'mc-range-end');
          copyRangeBegin.join('-') === date && (options.rangeClassName = 'mc-range-begin');
        }
        if (year === copyRangeEnd[0] && month + 1 === copyRangeEnd[1] && day === copyRangeEnd[2] - 1) {
          options.rangeClassName = options.rangeClassName ? ['mc-range-begin', 'mc-range-second-to-last'] : 'mc-range-second-to-last';
        }
        if (this.rangeBegin.length) {
          var beginTime = +new Date(this.rangeBegin[0], this.rangeBegin[1], this.rangeBegin[2]);
          var endTime = +new Date(this.rangeEnd[0], this.rangeEnd[1], this.rangeEnd[2]);
          var stepTime = +new Date(year, month, i);
          if (beginTime <= stepTime && endTime >= stepTime) {
            options.selected = true;
          }
        }
        if (this.begin.length) {
          var _beginTime = +new Date(parseInt(this.begin[0]), parseInt(this.begin[1]) - 1, parseInt(this.begin[2]));
          if (_beginTime > +new Date(year, month, i)) {
            options.disabled = true;
          }
        }
        if (this.end.length) {
          var _endTime = Number(new Date(parseInt(this.end[0]), parseInt(this.end[1]) - 1, parseInt(this.end[2])));
          if (_endTime < Number(new Date(year, month, i))) {
            options.disabled = true;
          }
        }
        if (playload && !weekSwitch) {
          options.disabled = true;
        } else if (this.disabled.length && disabledFilter(this.disabled)) {
          options.disabled = true;
        }
        var monthFirstDay = year + '-' + (month + 1) + '-' + 1;
        var monthLastDay = year + '-' + (month + 1) + '-' + new Date(year, month + 1, 0).getDate();
        monthFirstDay === date && options.selected && !options.rangeClassName && (options.rangeClassName = 'mc-range-month-first');
        monthLastDay === date && options.selected && !options.rangeClassName && (options.rangeClassName = 'mc-range-month-last');
        this.isCurrentMonthToday(options) && (options.isToday = true);
        !weekSwitch && playload && (options.selected = false);
        return options;
      } else if (this.multi) {
        var _options = void 0;
        if (this.value.find(function (v) {
          return year === v[0] && month === v[1] - 1 && i === v[2];
        })) {
          _options = __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()({ day: i, selected: true }, this.getLunarInfo(year, month + 1, i), this.getEvents(year, month + 1, i));
        } else {
          _options = __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()({ day: i, selected: false }, this.getLunarInfo(year, month + 1, i), this.getEvents(year, month + 1, i));
          if (this.begin.length) {
            var _beginTime2 = +new Date(parseInt(this.begin[0]), parseInt(this.begin[1]) - 1, parseInt(this.begin[2]));
            if (_beginTime2 > +new Date(year, month, i)) {
              _options.disabled = true;
            }
          }
          if (this.end.length) {
            var _endTime2 = +new Date(parseInt(this.end[0]), parseInt(this.end[1]) - 1, parseInt(this.end[2]));
            if (_endTime2 < +new Date(year, month, i)) {
              _options.disabled = true;
            }
          }
          if (playload && !weekSwitch) {
            _options.disabled = true;
          } else if (this.disabled.length && disabledFilter(this.disabled)) {
            _options.disabled = true;
          }
        }
        if (_options.selected && this.multiDaysData.length !== this.value.length) {
          this.multiDaysData.push(_options);
        }
        this.isCurrentMonthToday(_options) && (_options.isToday = true);
        !weekSwitch && playload && (_options.selected = false);
        return _options;
      } else {
        var _options2 = {};
        var monthHuman = month + 1;
        if (seletSplit[0] === year && seletSplit[1] === monthHuman && seletSplit[2] === i) {
          __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(_options2, { day: i, selected: true }, this.getLunarInfo(year, monthHuman, i), this.getEvents(year, monthHuman, i));
        } else {
          __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(_options2, { day: i, selected: false }, this.getLunarInfo(year, monthHuman, i), this.getEvents(year, monthHuman, i));
          if (this.begin.length) {
            var _beginTime3 = +new Date(parseInt(this.begin[0]), parseInt(this.begin[1]) - 1, parseInt(this.begin[2]));
            if (_beginTime3 > Number(new Date(year, month, i))) {
              _options2.disabled = true;
            }
          }
          if (this.end.length) {
            var _endTime3 = +new Date(parseInt(this.end[0]), parseInt(this.end[1]) - 1, parseInt(this.end[2]));
            if (_endTime3 < +new Date(year, month, i)) {
              _options2.disabled = true;
            }
          }
          if (playload && !weekSwitch) {
            _options2.disabled = true;
          } else if (this.disabled.length && disabledFilter(this.disabled)) {
            _options2.disabled = true;
          }
        }
        this.isCurrentMonthToday(_options2) && (_options2.isToday = true);
        !weekSwitch && playload && (_options2.selected = false);
        return _options2;
      }
    },
    isCurrentMonthToday: function isCurrentMonthToday(options) {
      var isToday = todayString === options.date;
      if (!isToday) return false;
      return this.weekSwitch ? isToday : Number(todayString.split('-')[1]) === this.month + 1;
    },
    watchRender: function watchRender(type) {
      var weekSwitch = this.weekSwitch;
      var daysDeepCopy = JSON.parse(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_json_stringify___default()(this.monthDays));
      if (type === 'events') {
        var events = this.events || {};
        __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys___default()(events).forEach(function (value) {
          daysDeepCopy.some(function (v) {
            return v.some(function (vv) {
              if (vv.date === value) {
                vv.eventName = events[value];
                return true;
              }
            });
          });
        });
        this.monthDays = daysDeepCopy;
      } else if (type === 'disabled') {
        var disabled = this.disabled || [];
        disabled.forEach(function (value) {
          daysDeepCopy.some(function (v) {
            return v.some(function (vv) {
              if (vv.date === value) {
                vv.disabled = true;
                return true;
              }
            });
          });
        });
      } else if (type === 'almanacs') {
        var almanacs = this.almanacs || {};
        __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys___default()(almanacs).forEach(function (value) {
          daysDeepCopy.some(function (v) {
            return v.some(function (vv) {
              if (vv.date.slice(5, 20) === value) {
                vv.lunar = almanacs[value];
                return true;
              }
            });
          });
        });
      } else if (type === 'tileContent') {
        var tileContent = this.tileContent || [];
        tileContent.forEach(function (value) {
          daysDeepCopy.some(function (v) {
            return v.some(function (vv) {
              if (vv.date === value.date) {
                vv.className = value.className;
                vv.content = value.content;
                return true;
              }
            });
          });
        });
      }
      if (weekSwitch) {
        this.monthDays = daysDeepCopy;
        this.days = [daysDeepCopy[this.weekIndex]];
      } else {
        this.days = daysDeepCopy;
      }
    },
    render: function render(y, m, renderer, payload) {
      var _this = this;

      var weekSwitch = this.weekSwitch;
      var isCustomRender = renderer === 'CUSTOMRENDER';
      var isWatchRenderValue = renderer === '_WATCHRENDERVALUE_';
      if (renderer === '_WATCHRENDER_') return this.watchRender(payload);
      if (isWatchRenderValue && weekSwitch) {
        this.positionWeek = true;
      }
      if (isCustomRender) {
        this.year = y;
        this.month = m;
        this.positionWeek = true;
        if (weekSwitch && !payload) {
          this.startWeekIndex = 0;
          this.weekIndex = 0;
        }
        this.updateHeadMonth();
      }
      var firstDayOfMonth = new Date(y, m, 1).getDay();
      var lastDateOfMonth = new Date(y, m + 1, 0).getDate();
      var lastDayOfLastMonth = new Date(y, m, 0).getDate();
      this.year = y;
      var i = 1,
          line = 0,
          temp = [],
          nextMonthPushDays = 1;
      for (i; i <= lastDateOfMonth; i++) {
        var day = new Date(y, m, i).getDay();
        var k = void 0;
        if (day === 0) {
          temp[line] = [];
        } else if (i === 1) {
          temp[line] = [];
          k = lastDayOfLastMonth - firstDayOfMonth + 1;
          for (var j = 0; j < firstDayOfMonth; j++) {
            //generate prev month surplus option
            temp[line].push(__WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(this.renderOption(this.computedPrevYear(), this.computedPrevMonth(), k, 'prevMonth'), { lastMonth: true }));
            k++;
          }
        }

        temp[line].push(this.renderOption(this.year, this.month, i)); //generate current month option

        if (day === 6 && i < lastDateOfMonth) {
          line++;
        } else if (i === lastDateOfMonth) {
          var _k = 1;
          var lastDateOfMonthLength = this.monFirst ? 7 : 6;
          for (var d = day; d < lastDateOfMonthLength; d++) {
            //generate next month surplus option
            temp[line].push(__WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(this.renderOption(this.computedNextYear(), this.computedNextMonth(), _k, 'nextMonth'), { nextMonth: true }));
            _k++;
          }
          nextMonthPushDays = _k;
        }
      }
      var completion = this.completion;
      if (this.monFirst) {
        if (!firstDayOfMonth) {
          var lastMonthDay = lastDayOfLastMonth;
          var LastMonthItems = [];
          for (var _i = 1; _i <= 7; _i++) {
            LastMonthItems.unshift(__WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(this.renderOption(this.computedPrevYear(), this.computedPrevMonth(), lastMonthDay), { disabled: false, lastMonth: true }));
            lastMonthDay--;
          }
          temp.unshift(LastMonthItems);
        }
        temp.forEach(function (item, index) {
          if (!index) {
            return item.splice(0, 1);
          };
          temp[index - 1].length < 7 && temp[index - 1].push(item.splice(0, 1)[0]);
        });
        if (!completion && !weekSwitch) {
          var lastIndex = temp.length - 1;
          var secondToLastIndex = lastIndex - 1;
          var differentMonth = temp[lastIndex][0].date.split('-')[1] !== temp[secondToLastIndex][6].date.split('-')[1];
          differentMonth && temp.splice(lastIndex, 1);
        }
      }
      if (completion && !weekSwitch && temp.length <= 5 && nextMonthPushDays > 0) {
        for (var _i2 = temp.length; _i2 <= 5; _i2++) {
          temp[_i2] = [];
          var start = nextMonthPushDays + (_i2 - line - 1) * 7;
          for (var _d = start; _d <= start + 6; _d++) {
            temp[_i2].push(__WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()({ day: _d, disabled: true, nextMonth: true }, this.getLunarInfo(this.computedNextYear(), this.computedNextMonth(true), _d), this.getEvents(this.computedNextYear(), this.computedNextMonth(true), _d)));
          }
        }
      }
      if (this.tileContent.length) {
        temp.forEach(function (item, index) {
          item.forEach(function (v, i) {
            var contents = _this.tileContent.find(function (val) {
              return val.date === v.date;
            });
            if (contents) {
              var _ref = contents || {},
                  className = _ref.className,
                  content = _ref.content;

              v.className = className;
              v.content = content;
            }
          });
        });
      }
      this.monthDays = temp;
      if (weekSwitch) {
        if (this.positionWeek) {
          var payloadDay = '';
          var searchIndex = true;
          if (Array.isArray(payload)) {
            //range
            payloadDay = [payload[0], payload[1] + 1, payload[2]].join('-');
          } else if (this.multi || isWatchRenderValue) {
            payloadDay = this.thisTimeSelect;
          }
          if (payload === 'SETTODAY') {
            payloadDay = todayString;
          } else if (isCustomRender) {
            if (typeof payload === 'string') {
              payloadDay = [y, Number(m) + 1, payload].join('-');
              searchIndex = true;
            } else if (typeof payload === 'number') {
              var setIndex = payload > temp.length ? temp.length - 1 : payload;
              this.startWeekIndex = setIndex;
              this.weekIndex = setIndex;
              this.positionWeek = false;
              searchIndex = false;
            }
          }
          var positionDay = payloadDay || todayString;
          if (searchIndex) {
            temp.some(function (v, i) {
              var isWeekNow = v.find(function (vv) {
                return vv.date === positionDay;
              });
              if (isWeekNow) {
                _this.startWeekIndex = i;
                _this.weekIndex = i;
                return true;
              }
            });
          }
          this.positionWeek = false;
        }
        this.days = [temp[this.startWeekIndex]];
        if (this.initRender) {
          this.setMonthRangeofWeekSwitch();
          this.initRender = false;
        }
      } else {
        this.days = temp;
      }
      var todayText = '今';
      if (typeof this.now === 'boolean' && !this.now) {
        this.showToday = { show: false };
      } else if (typeof this.now === 'string') {
        this.showToday = {
          show: true,
          text: this.now || todayText
        };
      } else {
        this.showToday = {
          show: true,
          text: todayText
        };
      }
    },
    renderer: function renderer(y, m, w) {
      var renderY = y || this.year;
      var renderM = typeof parseInt(m) === 'number' ? m - 1 : this.month;
      this.initRender = true;
      this.render(renderY, renderM, 'CUSTOMRENDER', w);
      !this.weekSwitch && (this.monthsLoop = this.monthsLoopCopy.concat());
    },
    computedPrevYear: function computedPrevYear() {
      var value = this.year;
      if (this.month - 1 < 0) {
        value--;
      }
      return value;
    },
    computedPrevMonth: function computedPrevMonth(isString) {
      var value = this.month;
      if (this.month - 1 < 0) {
        value = 11;
      } else {
        value--;
      }
      if (isString) {
        return value + 1;
      }
      return value;
    },
    computedNextYear: function computedNextYear() {
      var value = this.year;
      if (this.month + 1 > 11) {
        value++;
      }
      return value;
    },
    computedNextMonth: function computedNextMonth(isString) {
      var value = this.month;
      if (this.month + 1 > 11) {
        value = 0;
      } else {
        value++;
      }
      if (isString) {
        return value + 1;
      }
      return value;
    },
    getLunarInfo: function getLunarInfo(y, m, d) {
      var lunarInfo = __WEBPACK_IMPORTED_MODULE_3__calendarinit_js__["a" /* default */].solar2lunar(y, m, d);
      var yearEve = '';
      if (lunarInfo.lMonth === 12 && lunarInfo.lDay === __WEBPACK_IMPORTED_MODULE_3__calendarinit_js__["a" /* default */].monthDays(lunarInfo.lYear, 12)) {
        yearEve = '除夕';
      }
      var lunarValue = lunarInfo.IDayCn;
      var Term = lunarInfo.Term;
      var isLunarFestival = false;
      var isGregorianFestival = false;
      if (this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay]) {
        lunarValue = this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay];
        isLunarFestival = true;
      } else if (this.festival.gregorian[m + "-" + d]) {
        lunarValue = this.festival.gregorian[m + "-" + d];
        isGregorianFestival = true;
      }
      var lunarInfoObj = {
        date: y + '-' + m + '-' + d,
        lunar: yearEve || Term || lunarValue,
        isLunarFestival: isLunarFestival,
        isGregorianFestival: isGregorianFestival,
        isTerm: !!yearEve || lunarInfo.isTerm
      };
      if (__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys___default()(this.almanacs).length) {
        __WEBPACK_IMPORTED_MODULE_2_babel_runtime_core_js_object_assign___default()(lunarInfoObj, {
          almanac: this.almanacs[m + "-" + d] || '',
          isAlmanac: !!this.almanacs[m + "-" + d]
        });
      }
      return lunarInfoObj;
    },
    getEvents: function getEvents(y, m, d) {
      if (__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_keys___default()(this.events).length == 0) return false;
      var eventName = this.events[y + "-" + m + "-" + d];
      var data = {};
      if (eventName != undefined) {
        data.eventName = eventName;
      }
      return data;
    },
    prev: function prev(e) {
      var _this2 = this;

      e && e.stopPropagation();
      var weekSwitch = this.weekSwitch;
      var changeMonth = function changeMonth(changed) {
        if (_this2.monthIndex === 1) {
          _this2.oversliding = false;
          _this2.month = 11;
          _this2.year = parseInt(_this2.year) - 1;
          _this2.monthIndex = _this2.monthIndex - 1;
        } else if (_this2.monthIndex === 0) {
          _this2.oversliding = true;
          _this2.monthIndex = 12;
          setTimeout(function () {
            return _this2.prev(e);
          }, 50);
          return _this2.updateHeadMonth('custom');
        } else if (_this2.monthIndex === 13) {
          _this2.month = 11;
          _this2.year = parseInt(_this2.year) - 1;
          _this2.monthIndex = _this2.monthIndex - 1;
        } else {
          _this2.oversliding = false;
          _this2.month = parseInt(_this2.month) - 1;
          _this2.monthIndex = _this2.monthIndex - 1;
        }
        _this2.updateHeadMonth('custom');
        _this2.render(_this2.year, _this2.month);
        typeof changed === 'function' && changed();
        var weekIndex = weekSwitch ? _this2.weekIndex : undefined;
        _this2.$emit('prev', _this2.year, _this2.month + 1, weekIndex);
      };
      if (!this.weekSwitch) return changeMonth();
      var changeWeek = function changeWeek() {
        _this2.weekIndex = _this2.weekIndex - 1;
        _this2.days = [_this2.monthDays[_this2.weekIndex]];
        _this2.setMonthRangeofWeekSwitch();
        _this2.$emit('prev', _this2.year, _this2.month + 1, _this2.weekIndex);
      };
      var currentWeek = (this.days[0] || [])[0] || {};
      if (currentWeek.lastMonth || currentWeek.day === 1) {
        var monthChenged = function monthChenged() {
          var lastMonthLength = _this2.monthDays.length;
          var startWeekIndex = currentWeek.lastMonth ? lastMonthLength - 1 : lastMonthLength;
          _this2.startWeekIndex = startWeekIndex;
          _this2.weekIndex = startWeekIndex;
          changeWeek();
        };
        changeMonth(monthChenged);
      } else {
        changeWeek();
      }
    },
    next: function next(e) {
      var _this3 = this;

      e && e.stopPropagation();
      var weekSwitch = this.weekSwitch;
      var changeMonth = function changeMonth() {
        if (_this3.monthIndex === 12) {
          _this3.oversliding = false;
          _this3.month = 0;
          _this3.year = parseInt(_this3.year) + 1;
          _this3.monthIndex = _this3.monthIndex + 1;
        } else if (_this3.monthIndex === 0 && _this3.month === 11) {
          _this3.oversliding = false;
          _this3.month = 0;
          _this3.year = parseInt(_this3.year) + 1;
          _this3.monthIndex = _this3.monthIndex + 1;
        } else if (_this3.monthIndex === 13) {
          _this3.oversliding = true;
          _this3.monthIndex = 1;
          setTimeout(function () {
            return _this3.next(e);
          }, 50);
          return _this3.updateHeadMonth('custom');
        } else {
          _this3.oversliding = false;
          _this3.month = parseInt(_this3.month) + 1;
          _this3.monthIndex = _this3.monthIndex + 1;
        }
        _this3.updateHeadMonth('custom');
        _this3.render(_this3.year, _this3.month);
        _this3.$emit('selectMonth', _this3.month + 1, _this3.year);
        var weekIndex = weekSwitch ? _this3.weekIndex : undefined;
        _this3.$emit('next', _this3.year, _this3.month + 1, weekIndex);
      };
      if (!this.weekSwitch) return changeMonth();
      var changeWeek = function changeWeek() {
        _this3.weekIndex = _this3.weekIndex + 1;
        _this3.days = [_this3.monthDays[_this3.weekIndex]];
        _this3.setMonthRangeofWeekSwitch();
        _this3.$emit('next', _this3.year, _this3.month + 1, _this3.weekIndex);
      };
      var currentWeek = (this.days[0] || [])[6] || {};
      if (currentWeek.nextMonth || currentWeek.day === new Date(this.year, this.month + 1, 0).getDate()) {
        var startWeekIndex = currentWeek.nextMonth ? 1 : 0;
        this.startWeekIndex = startWeekIndex;
        this.weekIndex = startWeekIndex;
        changeMonth();
      } else {
        changeWeek();
      }
    },
    select: function select(k1, k2, data, e) {
      var _this4 = this;

      e && e.stopPropagation();
      var weekSwitch = this.weekSwitch;
      if (data.lastMonth && !weekSwitch) {
        return this.prev(e);
      } else if (data.nextMonth && !weekSwitch) {
        return this.next(e);
      }
      if (data.disabled) return;
      (data || {}).event = (this.events || {})[data.date] || '';
      var selected = data.selected,
          day = data.day,
          date = data.date;

      var selectedDates = date.split('-');
      var selectYear = Number(selectedDates[0]);
      var selectMonth = selectedDates[1] - 1;
      var selectMonthHuman = Number(selectedDates[1]);
      var selectDay = Number(selectedDates[2]);;
      if (this.range) {
        if (this.rangeBegin.length === 0 || this.rangeEndTemp !== 0) {
          this.rangeBegin = [selectYear, selectMonth, selectDay];
          this.rangeBeginTemp = this.rangeBegin;
          this.rangeEnd = [selectYear, selectMonth, selectDay];
          this.thisTimeSelect = this.rangeEnd;
          this.rangeEndTemp = 0;
        } else {
          this.rangeEnd = [selectYear, selectMonth, selectDay];
          this.thisTimeSelect = [selectYear, selectMonth, selectDay];
          if (this.rangeBegin.join('-') === this.rangeEnd.join('-')) {
            return this.rangeEndTemp = 0;
          }
          this.rangeEndTemp = 1;
          if (+new Date(this.rangeEnd[0], this.rangeEnd[1], this.rangeEnd[2]) < +new Date(this.rangeBegin[0], this.rangeBegin[1], this.rangeBegin[2])) {
            this.rangeBegin = this.rangeEnd;
            this.rangeEnd = this.rangeBeginTemp;
          }
          var rangeDate = function rangeDate(date) {
            return date.map(function (v, k) {
              var value = k === 1 ? v + 1 : v;
              return _this4.zero ? _this4.zeroPad(value) : value;
            });
          };
          var begin = rangeDate(this.rangeBegin);
          var end = rangeDate(this.rangeEnd);
          this.value.splice(0, 1, begin);
          this.value.splice(1, 1, end);
          this.$emit('select', begin, end);
        }
        this.rangeBgHide = !this.rangeEndTemp || this.rangeBegin.join('-') === this.rangeEnd.join('-');
        this.positionWeek = true;
        this.render(this.year, this.month, undefined, this.thisTimeSelect);
      } else if (this.multi) {
        var filterDayIndex = this.value.findIndex(function (v) {
          return v.join('-') === date;
        });
        if (~filterDayIndex) {
          this.handleMultiDay = this.value.splice(filterDayIndex, 1);
        } else {
          this.value.push([Number(Number(selectedDates[0])), Number(selectedDates[1]), day]);
        }
        this.days[k1][k2].selected = !selected;
        if (this.monthDays[k1][k2].selected) {
          this.multiDaysData.push(data);
        } else {
          this.multiDaysData = this.multiDaysData.filter(function (item) {
            return item.date !== date;
          });
        }
        this.thisTimeSelect = date;
        this.$emit('select', this.value, this.multiDaysData);
      } else {
        var currentSelected = this.value.join('-');
        this.monthDays.some(function (v) {
          return !!v.find(function (vv) {
            if (vv.date === currentSelected) {
              vv.selected = false;
              return true;
            }
          });
        });
        this.days[k1][k2].selected = true;
        this.day = day;
        var selectDate = [selectYear, selectMonthHuman, selectDay];
        this.value[0] = selectYear;
        this.value[1] = selectMonthHuman;
        this.value[2] = selectDay;
        this.today = [k1, k2];
        this.$emit('select', selectDate, data);
      }
    },
    changeYear: function changeYear() {
      if (this.yearsShow) {
        this.yearsShow = false;
        return false;
      }
      this.yearsShow = true;
      this.years = [];
      for (var i = this.year - 5; i < this.year + 7; i++) {
        this.years.push(i);
      }
    },
    changeMonth: function changeMonth(value) {
      this.oversliding && (this.oversliding = false);
      this.yearsShow = false;
      this.month = value;
      this.render(this.year, this.month, 'CUSTOMRENDER', 0);
      this.updateHeadMonth();
      this.weekSwitch && this.setMonthRangeofWeekSwitch();
      this.$emit('selectMonth', this.month + 1, this.year);
    },
    selectYear: function selectYear(value) {
      this.yearsShow = false;
      this.year = value;
      this.render(this.year, this.month);
      this.$emit('selectYear', value);
    },
    setToday: function setToday() {

      var now = new Date();
      this.year = now.getFullYear();
      this.month = now.getMonth();
      this.day = now.getDate();
      this.positionWeek = true;
      this.render(this.year, this.month, undefined, 'SETTODAY');
      this.updateHeadMonth();
    },
    setMonthRangeofWeekSwitch: function setMonthRangeofWeekSwitch() {
      var _this5 = this;

      this.monthsLoop = this.monthsLoopCopy.concat();
      this.days[0].reduce(function (prev, current) {
        if (!prev) return;
        var prveDate = ((prev || {}).date || '').split('-');
        var prevYear = prveDate[0];
        var prevMonth = prveDate[1];
        var currentMonth = ((current || {}).date || '').split('-')[1];
        if (prevMonth === currentMonth) {
          return current;
        } else {
          var prevMonthText = _this5.months[prevMonth - 1];
          var currentMonthText = _this5.months[currentMonth - 1];
          _this5.monthsLoop[_this5.monthIndex] = prevMonthText + '~' + currentMonthText;
        }
      });
    },
    dateInfo: function dateInfo(y, m, d) {
      return __WEBPACK_IMPORTED_MODULE_3__calendarinit_js__["a" /* default */].solar2lunar(y, m, d);
    },
    zeroPad: function zeroPad(n) {
      return String(n < 10 ? '0' + n : n);
    },
    updateHeadMonth: function updateHeadMonth(type) {
      if (!type) this.monthIndex = this.month + 1;
      this.monthPosition = this.monthIndex * this.positionH;
      this.monthText = this.months[this.month];
    }
  }
});

/***/ }),

/***/ 89:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    ref: "calendar",
    staticClass: "mpvue-calendar"
  }, [_c('div', {
    staticClass: "calendar-tools"
  }, [_c('div', {
    staticClass: "calendar-prev",
    attrs: {
      "eventid": '0'
    },
    on: {
      "click": _vm.prev
    }
  }, [(!!_vm.arrowLeft) ? _c('img', {
    attrs: {
      "src": _vm.arrowLeft
    }
  }) : _c('i', {
    staticClass: "iconfont icon-arrow-left"
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "calendar-next",
    attrs: {
      "eventid": '1'
    },
    on: {
      "click": _vm.next
    }
  }, [(!!_vm.arrowRight) ? _c('img', {
    attrs: {
      "src": _vm.arrowRight
    }
  }) : _c('i', {
    staticClass: "iconfont icon-arrow-right"
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "calendar-info",
    attrs: {
      "eventid": '2'
    },
    on: {
      "click": function($event) {
        $event.stopPropagation();
        _vm.changeYear($event)
      }
    }
  }, [_c('div', {
    staticClass: "mc-month"
  }, [(_vm.isIos) ? _c('div', {
    class: ['mc-month-inner', _vm.oversliding ? '' : 'month-transition'],
    style: ({
      'top': _vm.monthPosition + _vm.unit
    })
  }, _vm._l((_vm.monthsLoop), function(m, i) {
    return _c('span', {
      key: i
    }, [_vm._v(_vm._s(m))])
  })) : _c('div', {
    staticClass: "mc-month-text"
  }, [_vm._v(_vm._s(_vm.monthText))])]), _vm._v(" "), _c('div', {
    staticClass: "mc-year"
  }, [_vm._v(_vm._s(_vm.year))])])]), _vm._v(" "), _c('table', {
    attrs: {
      "cellpadding": "5"
    }
  }, [_c('div', {
    staticClass: "mc-head"
  }, [_c('div', {
    staticClass: "mc-head-box"
  }, _vm._l((_vm.weeks), function(week, index) {
    return _c('div', {
      key: index,
      staticClass: "mc-week"
    }, [_vm._v(_vm._s(week))])
  }))]), _vm._v(" "), _c('div', {
    class: ['mc-body', {
      'mc-range-mode': _vm.range,
      'week-switch': _vm.weekSwitch
    }]
  }, _vm._l((_vm.days), function(day, k1) {
    return _c('tr', {
      key: k1,
      class: {
        'gregorianStyle': !_vm.lunar
      }
    }, _vm._l((day), function(child, k2) {
      return _c('td', {
        key: k2,
        staticClass: "mc-day",
        class: [{
          'selected': child.selected,
          'mc-today-element': child.isToday,
          'disabled': child.disabled,
          'mc-range-select-one': _vm.rangeBgHide && child.selected,
          'lunarStyle': _vm.lunar,
          'mc-range-row-first': k2 === 0 && child.selected,
          'month-last-date': child.lastDay,
          'month-first-date': 1 === child.day,
          'mc-range-row-last': k2 === 6 && child.selected
        }, child.className, child.rangeClassName],
        style: (_vm.itemStyle),
        attrs: {
          "eventid": '3-' + k1 + '-' + k2
        },
        on: {
          "click": function($event) {
            _vm.select(k1, k2, child, $event)
          }
        }
      }, [(_vm.showToday.show && child.isToday && (_vm.weekSwitch || !child.disabled)) ? _c('span', {
        staticClass: "mc-today calendar-date"
      }, [_vm._v(_vm._s(_vm.showToday.text))]) : _c('span', {
        class: [{
          'mc-date-red': k2 === (_vm.monFirst ? 5 : 0) || k2 === 6
        }, 'calendar-date']
      }, [_vm._v(_vm._s(child.day))]), _vm._v(" "), (!!child.content) ? _c('div', {
        staticClass: "slot-element"
      }, [_vm._v(_vm._s(child.content))]) : _vm._e(), _vm._v(" "), (child.eventName && !_vm.clean) ? _c('div', {
        staticClass: "mc-text remark-text"
      }, [_vm._v(_vm._s(child.eventName))]) : _vm._e(), _vm._v(" "), (child.eventName && _vm.clean) ? _c('div', {
        staticClass: "mc-dot"
      }) : _vm._e(), _vm._v(" "), (_vm.lunar && (!child.eventName || _vm.clean)) ? _c('div', {
        staticClass: "mc-text",
        class: {
          'isLunarFestival': child.isAlmanac || child.isLunarFestival, 'isGregorianFestival': child.isGregorianFestival, 'isTerm': child.isTerm
        }
      }, [_vm._v(_vm._s(child.almanac || child.lunar))]) : _vm._e(), _vm._v(" "), (_vm.range && child.selected) ? _c('div', {
        staticClass: "mc-range-bg"
      }) : _vm._e()])
    }))
  }))]), _vm._v(" "), _c('div', {
    staticClass: "mpvue-calendar-change",
    class: {
      'show': _vm.yearsShow
    }
  }, [(!_vm.weekSwitch) ? _c('div', {
    staticClass: "calendar-years"
  }, _vm._l((_vm.years), function(y, index) {
    return _c('span', {
      key: y,
      class: {
        'active': y === _vm.year
      },
      attrs: {
        "eventid": '4-' + index
      },
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.selectYear(y)
        }
      }
    }, [_vm._v(_vm._s(y))])
  })) : _vm._e(), _vm._v(" "), _c('div', {
    class: ['calendar-months', {
      'calendar-week-switch-months': _vm.weekSwitch
    }]
  }, _vm._l((_vm.months), function(m, i) {
    return _c('span', {
      key: m,
      class: {
        'active': i === _vm.month
      },
      attrs: {
        "eventid": '5-' + i
      },
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.changeMonth(i)
        }
      }
    }, [_vm._v(_vm._s(m))])
  }))])], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5b4e0b96", esExports)
  }
}

/***/ }),

/***/ 91:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('Calendar', {
    ref: "calendar",
    attrs: {
      "value": _vm.value,
      "events": _vm.events,
      "weekSwitch": _vm.weekSwitch,
      "lunar": "false",
      "clean": "",
      "range": _vm.range,
      "arrowLeft": _vm.arrowLeft,
      "tileContent": _vm.tileContent,
      "almanacs": _vm.almanacs,
      "eventid": '0',
      "mpcomid": '0'
    },
    on: {
      "next": _vm.next,
      "prev": _vm.prev,
      "select": _vm.select,
      "selectMonth": _vm.selectMonth,
      "selectYear": _vm.selectYear
    }
  }), _vm._v(" "), _c('div', {
    attrs: {
      "id": "head"
    }
  }), _vm._v(" "), (_vm.userData) ? _c('scroll-view', {
    style: ({
      height: _vm.scrollHeight - 100 + 'px'
    }),
    attrs: {
      "scroll-y": ""
    }
  }, _vm._l((_vm.userData), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "cardBar"
    }, [(_vm.userData[index].mode == 0) ? _c('div', {
      staticClass: "card",
      attrs: {
        "id": _vm.userData[index].mode,
        "eventid": '1-' + index
      },
      on: {
        "click": _vm.goToChart
      }
    }, [_c('div', {
      staticClass: "weui-flex flex"
    }, [_c('div', {
      staticClass: "weui-flex__item item1"
    }, [_vm._v("体重")]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex__item item2"
    }, [_c('div', {
      staticClass: "num"
    }, [_vm._v(_vm._s(_vm.userData[index].weight))]), _vm._v(" "), _c('div', {
      staticClass: "com"
    }, [_vm._v("Kg")])])]), _vm._v(" "), _c('div', {
      staticClass: "time"
    }, [_c('div', {
      staticClass: "timeNum"
    }, [_vm._v(_vm._s(_vm.userData[index].time))])])]) : _vm._e(), _vm._v(" "), (_vm.userData[index].mode == 1) ? _c('div', {
      staticClass: "card",
      attrs: {
        "id": _vm.userData[index].mode,
        "eventid": '2-' + index
      },
      on: {
        "click": _vm.goToChart
      }
    }, [_c('div', {
      staticClass: "weui-flex flex"
    }, [_c('div', {
      staticClass: "weui-flex__item item1"
    }, [_vm._v("收缩压(DP)")]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex__item item2"
    }, [_c('div', {
      staticClass: "num"
    }, [_vm._v(_vm._s(_vm.userData[index].DP))]), _vm._v(" "), _c('div', {
      staticClass: "com"
    }, [_vm._v("mmHg")])])]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex flex"
    }, [_c('div', {
      staticClass: "weui-flex__item item1"
    }, [_vm._v("舒张压(SP)")]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex__item item2"
    }, [_c('div', {
      staticClass: "num"
    }, [_vm._v(_vm._s(_vm.userData[index].SP))]), _vm._v(" "), _c('div', {
      staticClass: "com"
    }, [_vm._v("mmHg")])])]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex flex"
    }, [_c('div', {
      staticClass: "weui-flex__item item1"
    }, [_vm._v("脉搏")]), _vm._v(" "), _c('div', {
      staticClass: "weui-flex__item item2"
    }, [_c('div', {
      staticClass: "num"
    }, [_vm._v(_vm._s(_vm.userData[index].Pulse))]), _vm._v(" "), _c('div', {
      staticClass: "com"
    }, [_vm._v("bpm")])])]), _vm._v(" "), _c('div', {
      staticClass: "time"
    }, [_c('div', {
      staticClass: "timeNum"
    }, [_vm._v(_vm._s(_vm.userData[index].time))])])]) : _vm._e()])
  })) : _vm._e(), _vm._v(" "), (_vm.showAddDate) ? _c('img', {
    staticClass: "addBtn",
    attrs: {
      "src": "/images/addBtn.svg",
      "eventid": '3'
    },
    on: {
      "click": _vm.addDate
    }
  }) : _vm._e(), _vm._v(" "), (!_vm.showAddDate) ? _c('img', {
    staticClass: "addBtn",
    attrs: {
      "src": "/images/todayBtn.svg",
      "eventid": '4'
    },
    on: {
      "click": _vm.setToday
    }
  }) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5daef28f", esExports)
  }
}

/***/ })

},[75]);
//# sourceMappingURL=main.js.map